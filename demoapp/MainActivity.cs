﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Widget;
using AndroidX.AppCompat.App;
using AndroidX.Core.Content;
using System;
using System.IO;

namespace demoapp
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource

            SetContentView(Resource.Layout.activity_main);

            Button btn = (Button)FindViewById(Resource.Id.button1);
            btn.Click += Btn_Click;
        }

        private void Btn_Click(object sender, System.EventArgs e) {
            string location = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDocuments).AbsolutePath;
            if (location.Contains("/Android")) {
                location = location.Split("/Android")[0];
            }
            location += "/Exports container";
            if (!Directory.Exists(location)) {
                Directory.CreateDirectory(location);
            }

            Intent intent = new Intent(Intent.ActionView);
            var contentURI = FileProvider.GetUriForFile(Application.Context, Application.Context.PackageName + ".fileprovider", new Java.IO.File(location));
            intent.SetDataAndType(contentURI, "resource/folder");

            intent.SetFlags(ActivityFlags.NewTask);
            intent.SetFlags(ActivityFlags.GrantReadUriPermission);
            try {
                StartActivity(intent);
            } catch (Exception ex) {
                Toast.MakeText(Application.Context, ex.Message, ToastLength.Long).Show();
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}